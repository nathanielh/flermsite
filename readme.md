#Flermsite2019

Flermengers website. Requires apache+php. Disgusting, I know.

##Building

The only thing in the build pipeline is a sass compiler. Run the code below to generate the site's style.css

`npm install`

`gulp sass`

##Development

`gulp`

will launch a php server and watch the sass directory for changes, recompiling the styles as appropriate. The dev server uses the php built-in server router, so it may behave slightly differently than running it on apache+php.

##Upload

Everything in the site directory goes onto your webserver. Don't miss the .htaccess file!
