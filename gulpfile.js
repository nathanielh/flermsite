'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var connect = require('gulp-connect-php');
var browserSync = require('browser-sync');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
  return gulp.src('./src/style.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest('./site'));
});

gulp.task('connect', function() {
  connect.server({
    base: "site",
    open: true,
    router: "./site/router.php"
  }, function (){
    browserSync({
      proxy: '127.0.0.1:8000'
    });
  });

  gulp.watch('site/*').on('change', function () {
    browserSync.reload();
  });
});

gulp.task('sass:watch', function () {
  gulp.watch('./src/style.scss', ['sass']);
});

gulp.task('default', ['sass:watch', 'connect']);
