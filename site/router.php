<?php
//echo"<pre>";
//var_dump($_SERVER);
//return true;

$request = $_SERVER['REQUEST_URI'];
if (preg_match('/\.(?:png|jpg|jpeg|gif|svg|css|js)$/', $request)) {
  return false;
} else {
  $title = "Destiny Clan On PC, XBox and PS4";


  if ($request == "" || $request == "/") {
    require __DIR__.'/partials/header.php';
    require __DIR__.'/views/index.php';
    require __DIR__.'/partials/footer.php';
  } elseif ($request == "/join") {
    $title = "Join Us";
    require __DIR__.'/partials/header.php';
    require __DIR__.'/views/join.php';
    require __DIR__.'/partials/footer.php';
  } elseif ($request == "/submit") {
    $title = "Welcome!";
    require __DIR__.'/partials/header.php';
    require __DIR__.'/views/submit.php';
    require __DIR__.'/partials/footer.php';

  } elseif (substr($request, -5, 5) == ".html") {
    header('Location: '.substr($request, 0, -5));
  } elseif (substr($request, -1, 1) == "/") {
    header('Location: '.substr($request, 0, -1));
  } else {
    header("HTTP/1.0 404 Not Found");
    require __DIR__.'/partials/header.php';
    require __DIR__.'/views/404.php';
    require __DIR__.'/partials/footer.php';
  }


}
?>
