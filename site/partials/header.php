<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Erhmahgerd Flermerngers: The Friendliest, Most Fabulous Destiny Clan On PC, XBox and PS4">
  <title>Flermerngers | <?php echo $title ?></title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
  <link rel="stylesheet" href="./style.css">
</head>
<body>

  <main class="wrapper">

    <nav id="menu-bar">
      <section class="container">
        <a class="navigation-title" href="/">
          <img src="img/flerm-header.svg" alt="Flermenger Logo" id="logo">
        </a>
        <ul class="navigation-list float-right">
          <li><a href="#" class="navigation-list-link">Link?</a></li>
          <li><a href="#" class="navigation-list-link">Link?</a></li>
          <li><a href="join.html" class="button">Join Us</a></li>
        </ul>
      </section>
    </nav>
