<footer id="footer">
  <section class="container">
    <p><small>What are you looking for down here? It's just boring legal stuff.</small></p>
  </section>
</footer>

</main>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="js/jquery.fittext.js"></script>
<script src="js/slick.js"></script>
<script>

//todo: move scripts into views
//home page scripts
jQuery("#title .big").fitText(0.79);
jQuery("#title .small").fitText(1.4);
jQuery("#byline").fitText(5, { minFontSize: '16px'});
$('#testimonials-carousel').slick({
  dots: true,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 2,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

//form scripts
jQuery.validate({
  borderColorOnError: ""
});


</script>
</body>
</html>
