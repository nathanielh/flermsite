<section id="splash">
	<div class="splash-image">&nbsp;</div>
	<div class="container">
		<div class="splash-text-wrapper">
			<div class="splash-text">
				<h1 id="title"><span class="small">Erhmahgerd </span><span class="big">Flermerngers</span></h1>
				<h2 id="byline">The Friendliest, Most Fabulous Destiny Clan <br />On PC, XBox and PS4</h2>
			</div>
		</div>
	</div>
</section>

<section id="intro" class="page-section"><div class="container">
	<p class="lead">Heyo! We're Erhmahgerd Flermerngers, or Flermerngers for short - the Destiny-focused Pink-Clad Clan™. You can find us on PC, XBox and PS4.</p>
	<a href="#" class="button join-button">Join Us</a>

	<div class="clan-link-row">
		<a href="#" class="clan-link"><img src="img/icons/windows.svg" alt="Windows Clan"></a>
		<a href="#" class="clan-link"><img src="img/icons/xbox.svg" alt="XBox Clan"></a>
		<a href="#" class="clan-link"><img src="img/icons/ps.svg" alt="PS4 Clan"></a>
	</div>

</div></section>

<section id="about-us" class="page-section"><div class="container">
	<h3>About Us</h3>

	<div class="row">
		<div class="column about-us-text">
			<div class="row">
				<div class="column">
					<h4>Everybody is Welcome</h4>
					<p>Regardless of whether you're a total noob or a D1 pro, you've got a home with the Flerms. We welcome everybody, regardless of skill level!</p>
				</div>
				<div class="column">
					<h4>Not Just Destiny!</h4>
					<p>Mostly we play Destiny, but we also have active channels for Overwatch, Minecraft, Monster Hunter World, Pokemon, Fallout 76, and more!</p>
				</div>
			</div>
			<div class="row">
				<div class="column">
					<h4>We Wear Pink</h4>
					<p>Our schtick is that we wear pink in game, because we think it's fun. (If you want to wear pink IRL, that's up to you. Send photos.)</p>
				</div>
				<div class="column">
					<h4>Bro, Do You Even Discord?</h4>
					<p>We coordinate games in our Discord server. Find groups, discuss strats, and talk smack! Use our join form (linked above) to get an invite.</p>
				</div>
			</div>
		</div>

		<div class="column about-us-picture">
			<img src="img/heart.png" alt="">
		</div>
	</div>


</div></section>

<section id="events" class="page-section"><div class="container">
	<h3>Events</h3>
	<p>Due to the size of our clan, it's easy to find groups to play Destiny with, no matter the time or the skill level! Across the clan we have achieved over 3600 raid completions. It's easy to find groups for strikes, patrols, PVP, quests, and other secrets that Destiny 2: Forsaken has to offer.</p>
	<p>We welcome people of all skill levels, whether you’re a casual player or if you’ve been a hardcore player since Destiny 1 launched. If you're a diehard player then take a look at our game records and see if you can break them!</p>
	<div class="row">
		<div class="column">
			<h4>Fastest Normal SoS Clear</h4>
			<p class="big-number">25 min</p>
		</div>
		<div class="column">
			<h4>Highest Nightfall Score</h4>
			<p class="big-number">320999</p>
		</div>
		<div class="column">
			<h4>Flawless Trials Runs</h4>
			<p class="big-number">141</p>
		</div>
	</div>
</div></section>

<section id="testimonials" class="page-section"><div class="container">
	<h3>Testimonials</h3>
	<div id="testimonials-carousel">

		<div class="testimonial"><div class="testimonial-inner">
			<img src="https://placekitten.com/300/300" alt="Member Photo">
			<blockquote>
				<p>"Super friendly, easy-going clan with something for guardians of all stripes. Definitely give them a try if you're tired of playing solo or want to play with a group you can always rely on for fun times."</p>
				<p class="quote-author"><em>-SpaceDuck</em> <small>Member, Ermagerd Flermerngers (PC)</small></p>
			</blockquote>
		</div></div>

		<div class="testimonial"><div class="testimonial-inner">
			<img src="https://placekitten.com/300/300" alt="Member Photo">
			<blockquote>
				<p>"This clan really is the best. Everyone is so welcoming and open to playing, no matter your gaming proficiency! The discord is always active, even if you can't play, people really connect and so many friends to be made!!! Flermily For Life."</p>
				<p class="quote-author"><em>-fumblingred14</em> <small>Moderator, Ermagerd Flermerngers (Xbox)</small></p>
			</blockquote>
		</div></div>

		<div class="testimonial"><div class="testimonial-inner">
			<img src="https://placekitten.com/300/300" alt="Member Photo">
			<blockquote>
				<p>"Take it from a veteran player who is a total newb  to this clan.  Be an Erhmahgerd Flermernger, life is better - and more pink. Seriously though, chill people and always someone to game with as long as you're chill too."</p>
				<p class="quote-author"><em>-Spar1an_</em> <small>Member, Ermagerd Flermerngers (PS4)</small></p>
			</blockquote>
		</div></div>


	</div>
</div></section>

<section id="join-us" class="page-section"><div class="container">
	<h3>Like what you see?</h3>
	<p>If you like what you've seen of us so far, and think you'd like to join our flermily, click here to fill out an application! (It's painless, we promise.)</p>
	<a href="join.html" class="button join-button">Join Us</a>
</div></section>
