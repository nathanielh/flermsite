<?php
function _email_format($el) {
  if (array_key_exists ($el, $_POST)) {
    if (is_array($_POST[$el])) {
      return "<strong>".htmlspecialchars(implode(", ", $_POST[$el]))."</strong>";
    } else {
      return "<strong>".htmlspecialchars($_POST[$el])."</strong>";
    }
  } else {
    return "(empty)";
  }
}
$email_format = '_email_format';


$email_text = <<<EOT
<h1>New Join Request</h1>
<p>Gamertag, PSN, or Battletag: {$email_format("gamertag")}</p>
<p>Discord ID: {$email_format("discordID")}</p>
<p>Bungie Profile: <a href="{$email_format("bungieProfile")}">{$email_format("bungieProfile")}</a></p>
<p>Email: <a href="mailto:{$email_format("email")}">{$email_format("email")}</a></p>
<p>Platform(s) of choice: {$email_format("playPlatform")}</p>
<p>Preferred Platform: {$email_format("playPreferred")}</p>
<p>Something Interesting: {$email_format("factoid")}</p>
<p>How they heard about us: {$email_format("howYouHeard")}</p>
<p>Other games they're interested in: {$email_format("otherGames")}</p>
<p>Read our rules: {$email_format("readRules")}</p>
<p>Acknowledged they need to join discord: {$email_format("readDiscordRules")}</p>
EOT;

//echo $email_text;

$to = 'me@nathanielh.com';

$subject = "New Flerm Form: {$email_format("gamertag")}";

$headers = "From: system@flerm.me\r\n";
$headers .= "Reply-To: system@flerm.me\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
mail($to, $subject, $email_text, $headers);

?>
<section id="welcome" class="page-section"><div class="container">
  <h1>Congrats and Welcome!</h1>
  <p><strong>Ermahgerd - Welcome to the Flermerngers!</strong> Use the widget below to join our Discord server. If you already have a Discord account, be sure to click the "I already have an account" link -- otherwise entering your email address will create a second account.</p>
  <div class="row welcome-row">
    <div class="column">
      <iframe src="https://discordapp.com/widget?id=191819833002819584&amp;theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>
    </div>
    <div class="column">
      <p>(Or if the widget is down for some reason, you can use this button)</p>
      <a href="http://link.flerm.me/discord" class="button button-big">Join Discord!</a>
    </div>
  </div>
  <p>Your Flermling status should be approved within 24 hours after you join. (If you don't join, unfortunately we have too many people to chase you down by email or on Xbox, Blizzard, Discord, etc.) If we miss your join for some reason, message @mods in the #public channel.</p>
  <p>See you there!</p>
</div></section>
