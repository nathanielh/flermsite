<?php
function printCheckboxGroup($name, $values, $perColumn) {
	$name = $name."[]";
	$i = 0;
	echo '<div class="row"><div class="column">';
	foreach ($values as &$value) {
		$safeValue = str_replace('"', "'", $value);
		$safeID = preg_replace("/[^a-zA-Z0-9]+/", "", $value);
		$i++;
		if ($i > $perColumn) {
			$i = 1;
			echo '</div><div class="column">';
		}
		echo "<input type=\"checkbox\" name=\"$name\" value=\"$safeValue\" id=\"$safeID\"> <label class=\"label-inline\" for=\"$safeID\">$value</label><br>";
	}
	echo '</div></div>';
}
?>
<section id="join-us-form" class="page-section"><div class="container">
	<h1>So you want to be a Flermernger?</h1>
	<p class="lead"><strong>The Flermerngers are a team-based clan that plays together.</strong> If you don't actively seek out clanmates / friends when you play, we're not for you.</p>
	<p>But if you prefer to play with a fireteam, join us! You're only 3 steps away from membership in the most frabjous clan on the interwebs!</p>

	<hr>
	<h2>1. Accept Rules</h2>
	<p>We welcome chill adults who agree to follow our three simple rules...</p>

	<h3>1. Wear Pink</h3>
	<p>Flermerngers embrace their majestic awkwardness. If you're an emo Dead Orbit warrior or want to strut around in matching armor with shiny raid shaders, this is NOT the group for you. ¯\_(ツ)_/¯ Now that Destiny shaders are semi-permanent, you should be rocking pink shaders like <strong>Dawn and Dusk, Metallic Sunrise or Nebula Rose</strong> on as much of your gear as possible.​ Bonus points for pink/absurd emblems and transmat effects.</p>
	<h3>2. Participate</h3>
	<p>Our <strong>Discord server</strong> is the core of our clan. When we can't play games, we all enjoy chatting about them (or other random topics) with friendly people. Members should be present on Discord at least weekly, even if just to say that you're busy and not currently playing. Please post on Discord before you play cooperative content, especially before you go looking on other LFG sites. Add other clan members to your friends list and invite each other to party chat when appropriate. Recognize that gently recruiting new members is everyone's​ job, not just admins.</p>
	<h3>3. Don't Be a Dick</h3>
	<p>We have <strong>zero tolerance</strong> for assholes in chat. This includes general douchenozzles, super salty players, and people using language that's way too spicy for friendly play ("gay", "f*g", "n*gger", "c*nt", etc). This behavior gets one warning before a ban. Rage quitting will earn an immediate ban with no warning.</p>
</div>

<form class="container" action="submit" method="post">
	<fieldset class="checkbox-agree">
		<input type="checkbox" id="readRules" name="readRules" data-validation="required">
		<label class="label-inline" for="readRules">I'm 18+ and agree to follow the clan rules above.</label>
		<p class="helper-text"><small>(We understand that some people act more mature than their age, and some less.  If you act and sound 18+ and don't tell anyone you're not, you're probably fine.  We don't ask anyone their age unless they act or sound immature.)</small></p>
	</fieldset>

	<hr>
	<h2>2. Provide Contact Info</h2>

	<fieldset>
		<label>Platform(s) of choice:</label>
		<div class="row row-platform-choice">
			<div class="column">
				<input type="checkbox" name="playPlatform[]" value="playPC" id="playPC" data-validation="checkbox_group" data-validation-qty="min1"> <label class="label-inline" for="playPC">PC</label><br>
				<input type="checkbox" name="playPlatform[]" value="playPS4" id="playPS4"> <label class="label-inline" for="playPS4">PS4</label><br>
				<input type="checkbox" name="playPlatform[]" value="playXBOX" id="playXBOX"> <label class="label-inline" for="playXBOX">XBOX</label>
			</div>
			<div class="column">
				<input type="radio" name="playPreferred" value="playPCPreferred" id="playPCPreferred" data-validation="required">
				<label class="label-inline" for="playPCPreferred">PC Preferred</label><br>

				<input type="radio" name="playPreferred" value="playPS4Preferred" id="playPS4Preferred">
				<label class="label-inline" for="playPS4Preferred">PS4 Preferred</label><br>

				<input type="radio" name="playPreferred" value="playXBOXPreferred" id="playXBOXPreferred">
				<label class="label-inline" for="playXBOXPreferred">XBOX Preferred</label>
			</div>
		</div>

		<p class="helper-text"><small>We're only interested in the platforms on which you want to team up to play multiplayer games.</small></p>
	</fieldset>

	<fieldset>
		<label for="gamertag">Gamertag, PSN, or Battletag:</label>
		<input type="text" id="gamertag" name="gamertag" data-validation="required">
		<p class="helper-text"><small>If you're submitting your Battletag (PC), please be sure to include the ID number, e.g. ​Lyme#2345</small></p>
	</fieldset>

	<fieldset>
		<label for="discordID">Discord User ID:</label>
		<input type="text" id="discordID" name="discordID" data-validation="required">
		<p class="helper-text"><small>Please enter your Discord ID including trailing number, e.g. Lyme#2345 (NOTE: This is not the same as your Battletag, at least the ID# is different.)&nbsp; If you don't have Discord yet, you'll have to <a href="https://discordapp.com/download" target="_blank" title="Download the Discord App">get it now</a>.</small></p>
	</fieldset>

	<fieldset>
		<label for="bungieProfile">Bungie.net Profile Link:</label>
		<input type="text" id="bungieProfile" name="bungieProfile">
		<p class="helper-text"><small>Please visit <a href="https://bungie.net" target="_blank" title="Takes you to bungie.net ! Login there to get your profile URL!">Bungie.net</a>, log-in, click your avatar at the top right, click View Profile, then copy/paste the link here, e.g. https://www.&#8203;bungie.net&#8203;/en/Profile&#8203;/254&#8203;/2462360&#8203;/Lyme (this step is optional, but if you don't provide it now, you'll have to send it to our mods later to get your clan invite).</small></p>
	</fieldset>

	<fieldset>
		<label for="email">Email:</label>
		<input type="email" id="email" name="email" data-validation="email">
		<p class="helper-text"><small>Your email will remain private - it's only used by the admins to contact you in case there's an issue during the join process or you are about to be kicked for inactivity.</small></p>
	</fieldset>

	<fieldset>
		<label>How did you hear about us?</label>
		<?php
		$values = array(
			"Weekly r/Fireteams recruitment post", "r/Fireteams Clan Recruitment Megathread", "Other Reddit",
			"Friend", "Recruited by a clan member (non-friend)", "Bungie Forums",
			"DotR Discord Server", "Web Search", "Other"
		);
		printCheckboxGroup("howYouHeard", $values, 5);
		?>
		<p class="helper-text"><small>Please check all that apply.</small></p>
	</fieldset>

	<fieldset>
		<label for="factoid">Tell Us Something Interesting About Yourself:</label>
		<textarea id="factoid" name="factoid" rows="8" cols="80" data-validation="required"></textarea>
		<p class="helper-text"><small>We like our fellow Flermerngers to have some personality. So tell us about yourself! (Seriously: We have denied membership for half-assing this answer.)</small></p>
	</fieldset>

	<fieldset>
		<label>Optional: If you are interested in playing other games with us, please let us know!</label>
		<?php
		$values = array(
			"FFXIV", "Minecraft", "Borderlands", "Fallout 76",
			"Monster Hunter World", "Overwatch", "Warframe",
			"Pokemon", "Tabletop RPGs", "Fortnite"
		);
		printCheckboxGroup("otherGames", $values, 4);
		?>
		<p class="helper-text"><small>If you don't see a game you'd like to play on this list, let us know! We're always adding new game groups.</small></p>
	</fieldset>

	<hr>
	<h2>3. Join Discord</h2>
	<p>Once you submit, you'll see a link to our Discord server. You'll join as a Flermling, then team up with some Flermerngers so they can vouch for you. Once you get the thumbs up from our members, your membership will be approved!</p>

	<fieldset class="checkbox-agree">
		<input type="checkbox" id="readDiscordRules" name="readDiscordRules" data-validation="required">
		<label class="label-inline" for="readDiscordRules">I understand that if I don't join Discord (or leave the server before approval), my membership will be denied with no follow-up.</label>
	</fieldset>

	<fieldset>
		<input class="button-primary" type="submit" value="Submit!">
	</fieldset>


</form></section>
